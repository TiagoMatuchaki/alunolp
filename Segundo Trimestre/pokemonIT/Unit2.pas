unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit3, Vcl.StdCtrls, Vcl.DBCtrls,
  Unit4;

type
  TForm2 = class(TForm)
    DBLookupListBox1: TDBLookupListBox;
    lb_id: TLabel;
    lb_treinador: TLabel;
    lb_nome: TLabel;
    lb_nivel: TLabel;
    Identificador: TDBText;
    Treinador: TDBText;
    Nome: TDBText;
    Nivel: TDBText;
    procedure btn_editarClick(Sender: TObject);
    procedure btn_inserirClick(Sender: TObject);
    procedure btn_deletarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.btn_deletarClick(Sender: TObject);
begin
DataModule3.FDQuery_Pokemon.Delete;
end;

procedure TForm2.btn_editarClick(Sender: TObject);
begin
  Form4.ShowModal();
end;

procedure TForm2.btn_inserirClick(Sender: TObject);
begin
  DataModule3.FDQuery_Pokemon.Append();
  Form4.ShowModal();
end;

end.
